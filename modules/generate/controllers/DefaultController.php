<?php

namespace app\modules\generate\controllers;

use app\modules\generate\services\OrderService;
use yii\console\Controller;
use yii\di\Instance;
use yii\queue\Queue;

class DefaultController extends Controller {
    /** @var Queue */
    private $queue;

    public function __construct($id, $module, $config = []) {
        $this->queue = Instance::ensure('queue', Queue::class);
        parent::__construct($id, $module, $config);
    }

    public function actionIndex() {
        $count = rand(1, 10);
        for ($i = 0; $i < $count; $i++)
            OrderService::createOrder();

        OrderService::createJob($this->queue, $count);

        return $this->stdout("create orders: $count");
    }
}