<?php

namespace app\modules\generate\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "orders".
 *
 * @property int         $id
 * @property int         $user_id    пользователь
 * @property int         $sum        сумма транакции
 * @property float       $commission коммисия
 * @property int|null    $status     статус
 * @property string|null $created_at создано
 * @property string|null $updated_at обновлено
 */
class Orders extends ActiveRecord {

    const STATUS_NEW     = 0; //новый
    const STATUS_SENDING = 1; //отправляется
    const STATUS_SEND    = 2; //отправленно

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'sum', 'commission'], 'required'],
            ['status', 'integer'],
            ['sum', 'integer', 'min' => 10, 'max' => 500],
            ['user_id', 'integer', 'min' => 1, 'max' => 20],
            [['commission'], 'number', 'min' => 0.5, 'max' => 2],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'         => 'ID',
            'user_id'    => 'User ID',
            'sum'        => 'Sum',
            'commission' => 'Commission',
            'status'     => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
