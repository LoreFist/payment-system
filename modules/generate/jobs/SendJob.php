<?php

namespace app\modules\generate\jobs;

use app\modules\generate\services\OrderService;
use yii\base\BaseObject;
use yii\queue\JobInterface;

/**
 * Class SendJob
 *
 * @package app\modules\generate\jobs
 */
class SendJob extends BaseObject implements JobInterface {

    public $countNewOrders;

    /**
     * @param \yii\queue\Queue $queue
     *
     * @return mixed|void
     */
    public function execute($queue) {
        $packet = OrderService::getPacket($this->countNewOrders);
        OrderService::sendPacket($packet);
    }
}