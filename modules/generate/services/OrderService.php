<?php

namespace app\modules\generate\services;

use app\modules\generate\jobs\SendJob;
use app\modules\generate\models\Orders;
use Yii;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\queue\Queue;

class OrderService {

    /**
     * генерациия комсы
     *
     * @param float $min
     * @param int   $max
     *
     * @return float
     */
    private static function generateCommission($min = 0.5, $max = 2) {
        $range = $max - $min;
        $num   = $min + $range * (mt_rand() / mt_getrandmax());
        $num   = round($num, 2);
        return ((float)$num);
    }

    /**
     * создает рандомный ордер
     *
     * @return Orders
     */
    public static function createOrder() {
        $orderModel             = new Orders();
        $orderModel->user_id    = rand(1, 20);
        $orderModel->sum        = rand(10, 500);
        $orderModel->commission = self::generateCommission();
        $orderModel->status     = Orders::STATUS_NEW;
        $orderModel->save();

        return $orderModel;
    }

    /**
     * создает очередь
     *
     * @param Queue $queue
     * @param       $count
     *
     * @return bool
     */
    public static function createJob(Queue $queue, $count) {
        try {
            $job = Yii::createObject([
                'class'          => SendJob::class,
                'countNewOrders' => $count,
            ]);

            return $queue->delay(20)->push($job) ? true : false;
        } catch (InvalidConfigException $e) {
            return false;
        }
    }

    /**
     * формируем пакет для отправки
     *
     * @param int $limit
     *
     * @return array
     */
    public static function getPacket($limit = 5) {
        $orders = Orders::find()->where(['status' => Orders::STATUS_NEW])->limit(5)->all();
        $packet = [];
        foreach ($orders as $order) {
            $order->status = Orders::STATUS_SENDING;
            $order->save();
            $packet[] = [
                'id'         => $order->id,
                'userId'     => $order->user_id,
                'sum'        => $order->sum,
                'commission' => $order->commission
            ];
        }

        return $packet;
    }

    public static function sendPacket($packet) {
        $client = new Client(
            [
                'baseUrl' => env('URL_RECEIVING') . '?crypt=' . self::getCrypt(),
            ]
        );
        try {
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setData($packet)
                ->send();

            if ($response->isOk) {
                foreach ($packet as $order)
                    Orders::updateAll(['status' => Orders::STATUS_SEND], ['id' => $order['id']]);
            }
        } catch (InvalidConfigException $e) {
            return false;
        }
    }

    /**
     * генерируем цифровую подпись
     *
     * @return string
     */
    public static function getCrypt() {
        return md5(date('Ymd'));
    }
}
