<?php

use yii\db\Migration;

/**
 * Class m200809_033002_orders
 */
class m200809_033002_orders extends Migration {
    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('orders', [
            'id'         => $this->primaryKey()->unsigned(),
            'user_id'    => $this->integer()->notNull()->comment('пользователь'),
            'sum'        => $this->smallInteger()->notNull()->comment('сумма транакции'),
            'commission' => $this->float()->notNull()->comment('коммисия'),
            'status'     => $this->smallInteger()->defaultValue(0)->comment('статус'),
            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->comment('создано'),
            'updated_at' => $this->timestamp()->defaultValue(null)->append('ON UPDATE CURRENT_TIMESTAMP')->comment('обновлено')
        ], null);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropTable('orders');
    }
}
