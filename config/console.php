<?php

$config = [
    'id'                  => 'basic-console',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'app\commands',
    'components'          => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log'   => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

    ],
    'controllerMap'       => [
        'fixture' => [
            'class' => 'yii\faker\FixtureController',
        ],
        'migrate' => [
            'class'               => 'yii\console\controllers\MigrateController',
            'migrationPath'       => [
                '@app/modules/generate/migrations',
            ],
            'migrationNamespaces' => [
                'yii\queue\db\migrations',
            ],
        ],
    ],
    'modules'             => [
        'generate' => [
            'class'               => \app\modules\generate\Module::class,
            'controllerNamespace' => 'app\modules\generate\controllers',
        ],
    ]
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][]    = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
